function countLetter(letter, sentence) {
  let result = 0;

  // Check first whether the letter is a single character.
  if (typeof letter === 'string' && letter.length === 1) {
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    for (let i = 0; i < sentence.length; i++) {
      if (sentence[i].toLowerCase() === letter.toLowerCase()) {
        result++;
      }
    }
    return result;
  } else {
    // If letter is invalid, return undefined.
    return undefined;
  }
}

function isIsogram(text) {
  // An isogram is a word where there are no repeating letters.
  // The function should disregard text casing before doing anything else.
  let letters = {};
  for (let i = 0; i < text.length; i++) {
    const letter = text[i].toLowerCase();
    if (letters[letter]) {
      // If the function finds a repeating letter, return false.
      return false;
    } else {
      letters[letter] = true;
    }
  }
  // If no repeating letters were found, return true.
  return true;
}

function purchase(age, price) {
  if (age < 13) {
    return undefined;
  } else if (age >= 13 && age <= 21) {
    const discountedPrice = price * 0.8;
    return Math.round(discountedPrice * 100) / 100 + '';
  } else if (age >= 65) {
    const discountedPrice = price * 0.8;
    return Math.round(discountedPrice * 100) / 100 + '';
  } else {
    return Math.round(price * 100) / 100 + '';
  }
}

function findHotCategories(items) {
  // Find categories that have no more stocks.
  // The hot categories must be unique; no repeating categories.
  const hotCategories = new Set();
  for (let i = 0; i < items.length; i++) {
    if (items[i].stocks === 0) {
      hotCategories.add(items[i].category);
    }
  }
  // Convert the Set to an array and return it.
  return Array.from(hotCategories);
}

function findFlyingVoters(candidateA, candidateB) {
  // Find voters who voted for both candidate A and candidate B.
  const votersA = new Set(candidateA);
  const flyingVoters = [];
  for (let i = 0; i < candidateB.length; i++) {
    if (votersA.has(candidateB[i])) {
      flyingVoters.push(candidateB[i]);
    }
  }
  // Return the array of flying voters.
  return flyingVoters.sort();
}

module.exports = {
  countLetter,
  isIsogram,
  purchase,
  findHotCategories,
  findFlyingVoters,
};
